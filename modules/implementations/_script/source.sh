#!/bin/bash


script_dir=$(dirname "${BASH_SOURCE[0]}")
script_dir=$(dirname "$0")
script_dir=$(dirname "$(readlink -f "$0")")

output=$(python3 -c '
import pathlib
this_directory = pathlib.Path (__file__).parent.resolve ()
print(this_directory)
')

echo $output
echo $script_dir