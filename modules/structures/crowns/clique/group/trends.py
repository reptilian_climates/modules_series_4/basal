
import crowns.modules.moves.trends.find as find_trend
import crowns.modules.moves.show as show
import crowns.modules.moves.trends.forget as forget_trend

def add ():
	import click
	@click.group ("trends")
	def group ():
		pass


	import click
	@group.command ("find")
	@click.option ('--name', required = True)
	def search (name):
		find_trend.find (name = name)
	
	@group.command ("show")
	def show ():
		show.show ()
	
	@group.command ("forget")
	@click.option ('--id', required = True)
	def forget_trend (id):
		forget_trend.forget (
			id = id
		)

	return group




#



