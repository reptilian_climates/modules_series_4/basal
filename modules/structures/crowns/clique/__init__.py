




#from .group import clique as clique_group
from crowns.mints import clique as mints_group
from crowns.treasuries import clique as treasuries_group

import crowns.modules.moves.save as save
import crowns.modules.moves.trends_to_mint as trends_to_mint
import crowns.modules.moves.on as on



import crowns

import click

import crowns.clique.group.trends as group_trends

def clique ():
	'''
		This configures the crowns module.
	'''
	crowns.start ()

	@click.group ()
	def group ():
		pass
	
	@group.command ("help")
	def help ():
		import pathlib
		from os.path import dirname, join, normpath
		this_directory = pathlib.Path (__file__).parent.resolve ()
		this_module = str (normpath (join (this_directory, "..")))

		import somatic
		somatic.start ({
			"directory": this_module,
			"extension": ".s.HTML",
			"relative path": this_module
		})
		
		import time
		while True:
			time.sleep (1)

	
	'''
		crowns on
	'''
	@group.command ("turn-on")
	def turn_on ():
		on.turn_on ()
	
	@group.command ("turn-off")
	def turn_off ():
		print ('not yet implemented')
		
	@group.command ("show")
	def show ():
		show.show ()

	
	'''
		crowns save --name folder
	'''
	@group.command ("save")
	@click.option ('--name', required = True)
	def save (name):
		save.save (
			name = name
		)


	'''
		crowns retrieve --name folder
	'''
	@group.command ("retrieve")
	@click.option ('--name', required = True)
	@click.option ('--id', required = False, default = '')
	def retrieve_command (name, id):
		trends_to_mint.start (
			name = name,
			id = id
		)


	group.add_command (mints_group.clique ())
	#group.add_command (treasuries_group.clique ())
	
	group.add_command (group_trends.add ())
	
	group ()




#
