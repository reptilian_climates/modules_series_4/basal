

def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_directory = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_directory, path)))

add_paths_to_system ([
	'modules'
])

	
import zipfile
import os	
import pathlib
from os.path import dirname, join, normpath
import sys
from pymongo import MongoClient
from bson.binary import Binary
from gridfs import GridFS


from grids.save import zip_and_save_to_gridfs

import io
import zipfile
import pymongo
from gridfs import GridFS

# Connect to MongoDB
client = pymongo.MongoClient('mongodb://localhost:27017/')
DB = client ['safety']
GridFS_collection = GridFS (DB, collection = 'zip_folders')
GridFS_collection_files = DB ['zip_folders.files']

'''
	DB:safety
		collection:passes 
		collection:zip_folders
'''
this_directory = pathlib.Path (__file__).parent.resolve ()	

id = zip_and_save_to_gridfs (
	folder_path = str (normpath (join (this_directory, 'folder'))), 
	metadata = {},
	GridFS_collection = GridFS_collection,
	GridFS_collection_files = GridFS_collection_files
)

#client ['safety'] ['zip_folders.files'].update ({'_id': id }, {'$set': {'uploadDate': '' }})

client ['safety'] ['zip_folders.files'].update_one({'_id': id }, {'$set': {'uploadDate': '' }})

DB ["passes"].insert_one ({
	"name": "company 1",
	"tags": [],
	"directories": [
		id
	]
})