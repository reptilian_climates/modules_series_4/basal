import re
from pymongo import MongoClient

# Connect to MongoDB
client = MongoClient('mongodb://localhost:27017/')
db = client['your_database']
collection = db['your_collection']

# Define the UTF-8 string to search for
search_string = 'unique'

# Define the aggregation pipeline
pipeline = [
    {
        '$match': {
            'content': {'$exists': True, '$type': 'binData'}
        }
    },
    {
        '$addFields': {
            'contentString': {'$toString': '$content'}
        }
    },
    {
        '$match': {
            'contentString': {'$regex': search_string, '$options': 'i'}
        }
    }
]

# Execute the aggregation pipeline
results = collection.aggregate(pipeline)

print ('results:', results)

# Print the search results
for result in results:
    print(result)
