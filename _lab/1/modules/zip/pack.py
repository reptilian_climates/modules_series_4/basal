

import zipfile
import os	
import pathlib
from os.path import dirname, join, normpath
import sys
from pymongo import MongoClient
from bson.binary import Binary
from gridfs import GridFS


def zip_pack (
	folder_path,
	zip_path
):
	try:
		with zipfile.ZipFile(zip_path, 'w', zipfile.ZIP_DEFLATED) as zipf:
			for root, dirs, files in os.walk(folder_path):
				for file in files:
					file_path = os.path.join(root, file)
					zipf.write(file_path, os.path.relpath(file_path, folder_path))
		return True
	except Exception as e:
		print(f"Error zipping folder: {e}")
		return False