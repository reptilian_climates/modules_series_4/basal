








def zip_folder_details (file_path):
	name = os.path.basename (file_path)
	size = os.path.getsize (file_path)
	
	with open (file_path, 'rb') as file:
		content = Binary (file.read())
		
	return {
		"name": name,
		"size": size,
		"content": content
	}

