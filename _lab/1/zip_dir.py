

'''
def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_directory = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_directory, path)))

add_paths_to_system ([
	'modules_pip',
	'modules'
])
'''

import pathlib
from os.path import dirname, join, normpath
import sys
this_directory = pathlib.Path (__file__).parent.resolve ()

import zipfile
import os
def zip_folder(folder_path, zip_path):
	try:
		with zipfile.ZipFile(zip_path, 'w', zipfile.ZIP_DEFLATED) as zipf:
			for root, dirs, files in os.walk(folder_path):
				for file in files:
					file_path = os.path.join(root, file)
					zipf.write(file_path, os.path.relpath(file_path, folder_path))
		return True
	except Exception as e:
		print(f"Error zipping folder: {e}")
		return False

# Specify the folder to be zipped and the path to save the zip file
folder_to_zip = str (normpath (join (this_directory, 'folder')))
zip_file_path = str (normpath (join (this_directory, 'folder'))) + '.zip'

# Zip the folder
if zip_folder(folder_to_zip, zip_file_path):
	print(f"Folder '{folder_to_zip}' zipped successfully to '{zip_file_path}'")
else:
	print("Failed to zip the folder.")

