
'''
	itinerary:
		steps:
			* zip the directory
			* save the zip to mongo with GridFS
			
			sanity:
			* retrieve the zip from GridFS
			* unzip the zip
			* check that the unzipped === the directory
'''


def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_directory = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_directory, path)))

add_paths_to_system ([
	'modules'
])

	
import zipfile
import os	
import pathlib
from os.path import dirname, join, normpath
import sys
from pymongo import MongoClient
from bson.binary import Binary
from gridfs import GridFS


from zip.pack import zip_pack


def zip_folder_details (file_path):
	name = os.path.basename (file_path)
	size = os.path.getsize (file_path)
	
	with open (file_path, 'rb') as file:
		content = Binary (file.read())
		
	return {
		"name": name,
		"size": size,
		"content": content
	}
	
def save_dir (
	directory_path = "",
	mongo_collection = None,
	DB = None
):	
	zip_file_path = directory_path + '.zip'
	zip_pack (directory_path, zip_file_path)
	zip_details = zip_folder_details (zip_file_path)
	
	mongo_collection.insert_one ({
		"directories": [
			zip_details
		]
	})
	
	fs = GridFS(DB, collection='folder_archives')
	fs.put(zip_details ["content"], filename=zip_details ["name"], metadata={})

	return;
	
	
# Connect to MongoDB
client = MongoClient('mongodb://localhost:27017/')
DB = client ['file_storage']
collection = DB ['files']
this_directory = pathlib.Path (__file__).parent.resolve ()
save_dir (
	directory_path = str (normpath (join (this_directory, 'folder'))),
	mongo_collection = collection,
	DB = DB
)		
		
