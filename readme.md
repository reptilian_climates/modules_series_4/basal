
******

Bravo!  You have received a Mercantilism Diploma in "crowns" from   
the Orbital Convergence University International Air and Water 
Embassy of the 🍊 Planet (the planet that is one ellipse further from
the Sun than Earth's ellipse).

You are now officially certified to include "crowns" in your practice.

******

--

# 👑 
# crowns

--

## 💎 obtain
`(ZSH) pip install crowns`

--

## 🪙 tutorial
`(ZSH) crowns shares`

--


