




#
#	how to publish:
#
#		python3 structures/modules/this_module/_status/status.proc.py
#		cp structures/modules/this_module/module.MD readme.md
#		(rm -rf dist && python3 -m build && twine upload dist/*)
#


def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_directory = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_directory, path)))

add_paths_to_system ([
	'modules_pip'
])