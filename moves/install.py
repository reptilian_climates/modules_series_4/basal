

'''
https://packaging.python.org/en/latest/guides/distributing-packages-using-setuptools/#create-an-account

#
#	apt install python3.10-venv
#	pip install twine
#
'''

def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_directory = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_directory, path)))

add_paths_to_system ([
	'modules_pip',
	'modules'
])


'''
	Caution, this removes the modules_pip, before installing the new ones.
'''
import rich
import semver
import toml

import os
import pathlib
from os.path import dirname, join, normpath
import sys
	
import ramps.install as install	
	
def start ():
	this_directory = pathlib.Path (__file__).parent.resolve ()	
	base_directory = normpath (join (this_directory, ".."))	
	
	pyproject_path = normpath (join (base_directory, "pyproject.toml"))
	pip_modules_path = normpath (join (base_directory, "modules/structures_pip"))

	install.install_dependencies (
		pyproject_path = pyproject_path,
		pip_modules_path = pip_modules_path
	)
	
start ()